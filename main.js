/** Initialize Canvas */
const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");

/** Canvas Environment Setting */
ctx.fillStyle = "white";
ctx.fillRect(0, 0, canvas.width, canvas.height);

// Relational Variable
let toolInUse = 'stroke';
let lastCursorPoint = { x: 0, y: 0 };
let cursorPoint = { x: 0, y: 0 };
let textingPoint = {};
let shapeOriginalPoint = {};
let isDrawing = false;
let isShaping = false;
let currentColor = "#000";
let currentBrushSize = 5;
let currentFont = 'Ariel';
let currentFontSize = 18;

let isClickedEvents = {
    isTexting: false
};

/** Color Picker Relation */
const colorPadGen = document.querySelector('#color-pad-gen');
const picker = new CP(colorPadGen, false, colorPadGen);
picker.on("change", (color) => {
    currentColor = '#' + color;
});
picker.set('rgb(255, 193, 94)');
picker.self.classList.add('static');
picker.enter();

/** Brush Size Detection */
const changeBrushSize = (value) => {
    currentBrushSize = value / 10;
};

/** Word Setting */
const fontChange = () => {
    const selectElement = document.getElementById('font');
    currentFont = selectElement.value;
};

const fontSizeChange = () => {
    const selectElement = document.getElementById('font-size');
    currentFontSize = selectElement.value;
};

// State Relation
let redoList = [];
let undoList = [];

const saveState = (list = undoList, keepRedo = false) => {
    console.log('in save');
    if(!keepRedo) redoList = [];
    list.push(canvas.toDataURL());
};

const restoreState = (pop, push) => {
    if (!pop.length) return;
    saveState(push, true);
    const restored = pop.pop();
    const img = document.createElement('img');
    img.src = restored;
    img.onload = () => {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height, 0, 0, canvas.width, canvas.height);
    };
};

const undo = () => {
    restoreState(undoList, redoList);
};

const redo = () => {
    restoreState(redoList, undoList);
};

const reset = () => {
    redoList = [];
    undoList = [];
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = "white";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
}

/** Camera Setting */
let blob;
let imageCapture;
let track;
const videoElement = document.querySelector('#video-camera');
const CAPTURE_WIDTH = 480;
const ratio = 16/9;

const openCameraPanel = async () => {
    document.querySelector('.modal').classList.remove('hide');
    const mediaStream = await navigator.mediaDevices.getUserMedia({ video: true, audio: false });
    videoElement.srcObject = mediaStream;
    videoElement.play();
    track = mediaStream.getVideoTracks()[0];
    imageCapture = new ImageCapture(track);
};

const takePhoto = async () => {
    blob = await imageCapture.takePhoto().catch(error => {
        console.error(error);
    });
    closeCameraPanel();
};

const putCamera = async () => {
    const bitmap = await createImageBitmap(blob, { resizeWidth: CAPTURE_WIDTH, resizeHeight: CAPTURE_WIDTH / ratio });
    ctx.drawImage(bitmap, cursorPoint.x, cursorPoint.y);
};

const closeCameraPanel = () => {
    document.querySelector('.modal').classList.add('hide');
    track.stop();
}

/** Mouse Detection */
canvas.addEventListener('mousemove', (event) => {
    // Remember the cursor point
    lastCursorPoint.x = cursorPoint.x;
    lastCursorPoint.y = cursorPoint.y;
    cursorPoint.x = event.offsetX;
    cursorPoint.y = event.offsetY;
    // Change to current color
    ctx.fillStyle = currentColor;
    ctx.strokeStyle = currentColor;
    ctx.lineWidth = currentBrushSize * 2;
    // Check whether the drawing is processing.
    if (isDrawing && drawingProcess[toolInUse]) drawingProcess[toolInUse]();
    // Check whether cursor need to change
    changeCursor();
});

canvas.addEventListener('mousedown', (event) => {
    isDrawing = true;
    // Pre-remember texting word place
    if(!isClickedEvents.isTexting) textingPoint = event;

    // Pre-remember Shape place
    shapeOriginalPoint = event;

    // Call Function
    if(makingElement[toolInUse]) makingElement[toolInUse]();

    // Keep State
    saveState();
});

canvas.addEventListener('mouseup', (event) => {
    isDrawing = false;
    isShaping = false;
});

/** Tool Select */
const selectTool = (toolName, toolId) => {
    const noClickBg = [ 'back', 'next', 'reset', 'download', 'upload' ];
    // console.log(toolName, noClickBg.some[toolName]);
    if (!noClickBg.includes(toolName)) {
        // Make toolset display normal
        toolInUse = toolName;
        const allTools = document.querySelectorAll('.toolset-icon');
        for (const tool of allTools) {
            tool.classList.remove('selected');   
        }
        const selectedTool = document.querySelector(`.tool-${toolId}`);
        selectedTool.classList.add('selected');
    }
    // Check one-time function
    const oneTimeProcess = {
        back: undo,
        next: redo,
        reset,
        camera: openCameraPanel
    };
    if (oneTimeProcess[toolName]) oneTimeProcess[toolName]();
};

/** Cursor Icon */
const changeCursor = () => {
    const cursorTypes = {
        stroke: "url('./assets/icon_cursor_pen.png'), auto",
        eraser: "url('./assets/icon_cursor_eraser.png'), auto",
        picture: "url('./assets/icon_cursor_picture.png'), auto",
        camera: "url('./assets/icon_cursor_picture.png'), auto",
        texting: "url('./assets/icon_cursor_text.png'), auto"
    }
    canvas.style.cursor = cursorTypes[toolInUse] ? cursorTypes[toolInUse] : 'default';
}

/** Shaping Relation */
let originData;
const rebackOrigin = () => {
    const originImg = document.createElement('img');
    originImg.src = originalData;
    originImg.onload = () => {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(originImg, 0, 0, canvas.width, canvas.height, 0, 0, canvas.width, canvas.height);
    };
};

/** Drawing Relation */
const stroke = () => {
    ctx.beginPath();
    ctx.moveTo(lastCursorPoint.x, lastCursorPoint.y);
    ctx.lineTo(cursorPoint.x, cursorPoint.y);
    ctx.closePath();
    ctx.stroke();
};

const eraser = () => {
    ctx.fillStyle = "#FFFFFF";
    ctx.fillRect(cursorPoint.x, cursorPoint.y, 30, 30);
};

const circle = () => {
    if (!isShaping) originalData = canvas.toDataURL();
    else rebackOrigin();
    const xOffset = Math.abs(cursorPoint.x - shapeOriginalPoint.offsetX);
    const yOffset = Math.abs(cursorPoint.y - shapeOriginalPoint.offsetY);
    const radius = Math.sqrt(Math.pow(xOffset, 2) + Math.pow(yOffset, 2));
    ctx.beginPath();
    ctx.arc(
        (shapeOriginalPoint.offsetX + cursorPoint.x) / 2,
        (shapeOriginalPoint.offsetY + cursorPoint.y) / 2,
        radius / 2,
        0,
        Math.PI * 2
    );
    setTimeout(() => { ctx.stroke(); }, 5);
    isShaping = true;
};

const triangle = () => {
    if (!isShaping) originalData = canvas.toDataURL();
    else rebackOrigin();
    ctx.beginPath();
    ctx.moveTo(
        (shapeOriginalPoint.offsetX + cursorPoint.x) / 2,
        shapeOriginalPoint.offsetY
    );
    ctx.lineTo(
        shapeOriginalPoint.offsetX,
        cursorPoint.y
    );
    ctx.lineTo(cursorPoint.x, cursorPoint.y);
    ctx.closePath();
    setTimeout(() => { ctx.stroke(); }, 5);
    isShaping = true;
};

const rec = () => {
    if (!isShaping) originalData = canvas.toDataURL();
    else rebackOrigin();
    setTimeout(() => {
        ctx.strokeRect(
            shapeOriginalPoint.offsetX,
            shapeOriginalPoint.offsetY,
            cursorPoint.x - shapeOriginalPoint.offsetX, 
            cursorPoint.y - shapeOriginalPoint.offsetY    
        );
    }, 5);
    isShaping = true;
}

const line = () => {
    if (!isShaping) originalData = canvas.toDataURL();
    else rebackOrigin();
    ctx.beginPath();
    ctx.moveTo(shapeOriginalPoint.offsetX, shapeOriginalPoint.offsetY);
    ctx.lineTo(cursorPoint.x, cursorPoint.y);
    setTimeout(() => { ctx.stroke(); }, 5);
    isShaping = true;
}

const texting = () => {
    let inputElement;
    if (isClickedEvents.isTexting) {
        // 表示編輯完成了
        inputElement = document.querySelector('.input-texting');
        ctx.font = `${currentFontSize}px ${currentFont}`;
        // ctx.fillStyle = 'black';
        ctx.fillText(inputElement.value, textingPoint.offsetX, textingPoint.offsetY);
        inputElement.remove();
        isClickedEvents.isTexting = false;
        return;
    }
    // Generating DOM
    inputElement = document.createElement('input');
    inputElement.className = "input-texting";
    const painterBoard = document.querySelector('.painter-board');
    painterBoard.appendChild(inputElement);
    // Adjust by css
    console.log(currentFontSize);
    inputElement.style.left = `${textingPoint.clientX}px`;
    inputElement.style.top = `${textingPoint.clientY}px`;
    inputElement.style.height = `${Number(currentFontSize) + 10}px`;
    inputElement.style.fontSize = `${currentFontSize}px`;
    isClickedEvents.isTexting = true;
};

document.addEventListener('keydown', (e) => {
    if(e.code === 'Enter' && isClickedEvents.isTexting) {
        texting();
    }
});

// Rainbow Pen Related
let currentStep = 0;
const ONE_MOVEMENT = 40;
const R = [255, 255, 255, 139, 131, 155];
const G = [85, 134, 233, 241, 178, 110];
const B = [94, 80, 129, 139, 255, 243];
let rainbowDirection = true;
const rainbow = () => {
    const ratio = currentStep / ONE_MOVEMENT;
    const floorRatio = Math.floor(ratio);
    const specRatio = ratio - floorRatio;
    const nowColor = {
        r: specRatio * R[floorRatio+1] + (1-specRatio) * R[floorRatio],
        g: specRatio * G[floorRatio+1] + (1-specRatio) * G[floorRatio],
        b: specRatio * B[floorRatio+1] + (1-specRatio) * B[floorRatio]
    };
    ctx.strokeStyle = `rgb(${nowColor.r},${nowColor.g},${nowColor.b})`;
    stroke();
    if(currentStep > 5*ONE_MOVEMENT - 3) rainbowDirection = false;
    else if (currentStep <= 0) rainbowDirection = true;
    
    if (rainbowDirection) currentStep++;
    else currentStep--;
};

// Upload Canvas Related
const canvasLoader = document.getElementById('imageLoader');
canvasLoader.addEventListener('change', (e) => {
    const reader = new FileReader();
    reader.onload = (event) => {
        const img = new Image();
        img.onload = () => {
            ctx.drawImage(img, 0, 0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
});


// Upload Picture Related
let keepPicutreEvent;
const imageLoader = document.getElementById('imageLoader-picture');
imageLoader.addEventListener('change', (e) => { keepPicutreEvent = e }, false);
const picture = () => {
    const reader = new FileReader();
    reader.onload = (event) => {
        const img = new Image();
        img.onload = () => {
            const ratio = img.height / img.width;
            ctx.drawImage(img, cursorPoint.x, cursorPoint.y, 400, 400*ratio);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(keepPicutreEvent.target.files[0]);
};

// Module Collection
const drawingProcess = {
    stroke,
    eraser,
    circle,
    triangle,
    rec,
    line,
    rainbow
};

const makingElement = {
    texting,
    picture,
    camera: putCamera
};

// Initialize
saveState();
selectTool('stroke', 1);