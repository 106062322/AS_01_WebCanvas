# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

# 小畫家
## 工具列介紹
**介紹順序將 由左至右 由上至下**
1. 畫筆 - Stroke
> 透過 ctx.stroke 追蹤滑鼠鼠標而成
2. 橡皮擦 - Eraser
3. 打字 - Texting
> 操作步驟
> 1. 於畫布上任一處點擊
> 2. 在 input 框中輸入文字
> 3. 輸入完成後，點擊畫布上任一處，或按下 Enter 鍵，即可完成編輯。
> 注意：可更改 字型 與 大小
4. 畫圓
> 按住鼠標後拖曳，即可畫出圓
5. 畫三角形
> 按住鼠標後拖曳，即可畫出三角形
6. 畫長方形
> 按住鼠標後拖曳，即可畫出長方形
7. 畫線
> 按住鼠標後拖曳，即可畫出線
8. 上傳畫布
> 此為上傳 所下載過的畫布，即會覆蓋過原先的圖。
9. 下載畫布
> 此為下載當前的畫布。
10. 上一步
11. 下一步
12. 重置
> 此將清除畫布中所顯示項目，並同時清空 上一步 與 下一步 的儲存資料。
13. 上傳圖片
> 可選擇想上傳的圖片，進行張貼
14. 自拍
> 此需要有攝影機的電腦
> 完成拍攝後，請於畫布上點擊，即可張貼所拍攝的相片。
15. 彩虹
> 按住鼠標後拖曳，即可畫出彩虹

## 相關畫布設定
1. Color Picker
> 引用 Color Picker 模組進行顏色設定
2. 文字選擇
> 可選擇 字型 與 字大小
3. 筆刷大小
